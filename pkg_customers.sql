CREATE OR REPLACE PACKAGE pkg_customers AS
    PROCEDURE add_new_customer 
      (p_first_name bank_customers.first_name%type,
       p_last_name bank_customers.last_name%type,
       p_father_name bank_customers.father_name%type,
       p_email bank_customers.email%type,
       p_mob_number bank_customers.mob_number%type,    
       p_country_name bank_countries.name%type,
       p_regdate bank_customers.regdate%type);
END;
/
CREATE OR REPLACE PACKAGE BODY pkg_customers AS
    PROCEDURE add_new_customer 
      (p_first_name bank_customers.first_name%type,
       p_last_name bank_customers.last_name%type,
       p_father_name bank_customers.father_name%type,
       p_email bank_customers.email%type,
       p_mob_number bank_customers.mob_number%type,    
       p_country_name bank_countries.name%type,
       p_regdate bank_customers.regdate%type) IS
       
       v_country_id bank_countries.id%type;
       v_local_country constant varchar2(100) :='Azerbaijan';
       v_resident number(1);
       v_datesdiffcount number;
    BEGIN
              --get country id 
       SELECT id INTO v_country_id FROM bank_countries
           WHERE name=v_local_country;
       --get dates diff
       SELECT sysdate-p_regdate into v_datesdiffcount FROM dual;
           
       --get resident or non-resident
       if UPPER(p_country_name) = UPPER(v_local_country)
           OR v_datesdiffcount>90  THEN  v_resident := 1;
        ELSE v_resident := 0;  
       END IF;
     
       --insert data
      INSERT INTO 
          bank_customers
           (first_name,
            last_name,
            father_name,
            email,
            mob_number,
            country_id,
            resident,
            regdate)
          VALUES(p_first_name,
                 p_last_name,
                 p_father_name,
                 p_email,
                 p_mob_number,
                 v_country_id,
                 v_resident,
                 p_regdate);
        
        COMMIT;
   END;
       
END;
/
